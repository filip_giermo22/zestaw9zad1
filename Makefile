CC=gcc
CLAGS=-Wall
LDLIBS=-lm

main: main.o klient.o
	$(CC) $(CFLAGS) -o main main.o klient.o $(LDLIBS)

main.o: main.c moduly.h
	$(CC) $(CFLAGS) -c main.c

klient.o: klient.c
	$(CC) $(CFLAGS) -c klient.c



